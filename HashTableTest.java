/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Gillian Wolff. 

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**Hashtable Test Class.*/
public class HashTableTest {

    /**Hashtable of strings used throughout testing.*/
    private HashTable<String> ht; 

    /**Set up before tests.*/
    @Before
    public void setup() {
        this.ht = new HashTable<String>(100);
    }

    /**Test to make sure that the hashtable is empty 
     * before anything is inserted. 
     */
    @Test
    public void preEmptiness() {
        assertTrue("Result", this.ht.isEmpty());
        ht.insert(50, "block"); 
        assertFalse("Result", this.ht.isEmpty());
    }

    /**Test the insert function.*/
    @Test
    public void testInsert() {
        this.ht.insert(50, "block 50");
        this.ht.insert(60, "block 60");
        this.ht.insert(22, "block 22");
        assertFalse("Result", this.ht.isEmpty());
    }

    /**Test to make sure the remove best function 
     * removes the easiest thing to remove that has 
     * a key >= the key that is being searched for. 
     */
    @Test
    public void testRemoveBest() {
        this.ht.insert(57, "block 57");
        this.ht.insert(59, "block 59");
        this.ht.insert(55, "block 55");
        this.ht.insert(52, "block 52");
        assertEquals("block 55", this.ht.removeBest(54)); 
        assertEquals("block 59", this.ht.removeBest(56));
        assertEquals(2, this.ht.getSize()); 
    }

    /**Test to make sure that if there is not a good block 
     * in the bucket that it hashes to that it goes to the 
     * look in the next bucket. 
     */
    @Test
    public void testRemoveBestNewBucket() {
        this.ht.insert(27, "block 27");
        this.ht.insert(39, "block 39");
        this.ht.insert(35, "block 35"); 
        assertEquals(11, this.ht.getCapacity());
        assertEquals("block 35", this.ht.removeBest(28));
        assertEquals(2, this.ht.getSize()); 
        assertEquals("block 39", this.ht.removeBest(28));
    }

    /**Test the rehash function to make sure it 
     * rehashes when the load factor is > .75. 
     */
    @Test
    public void testRehash() { 
        assertEquals(11, this.ht.getCapacity());
        this.ht.insert(3, "block 3");
        this.ht.insert(13, "block 13");
        this.ht.insert(23, "block 23");
        this.ht.insert(33, "block 33");
        this.ht.insert(43, "block 43");
        this.ht.insert(53, "block 53");
        this.ht.insert(63, "block 63");
        this.ht.insert(73, "block 73");
        this.ht.insert(76, "block 76");
        assertEquals(9, this.ht.getSize()); 
        assertEquals(25, this.ht.getCapacity());
    }

    /**Test remove best after a rehash occurs.*/
    @Test
    public void testRehashRemoveBest1() {
        this.ht.insert(57, "block 57");
        this.ht.insert(59, "block 59");
        this.ht.insert(55, "block 55");
        this.ht.insert(52, "block 52");
        this.ht.insert(51, "block 51");
        this.ht.insert(53, "block 53");
        this.ht.insert(54, "block 54");
        this.ht.insert(58, "block 58");
        this.ht.insert(22, "block 22");
        assertEquals(25, this.ht.getCapacity()); 
        assertEquals(9, this.ht.getSize());
        assertEquals("block 52", this.ht.removeBest(50));
        assertEquals("block 57", this.ht.removeBest(55));
        assertEquals("block 53", this.ht.removeBest(53)); 
        assertEquals(6, this.ht.getSize()); 
    }

    /**Test that it can move up to the next block to remove best.*/
    @Test
    public void testRehashRemoveBest2() {
        this.ht.insert(67, "block 67");
        this.ht.insert(29, "block 29");
        this.ht.insert(35, "block 35");
        this.ht.insert(12, "block 12");
        this.ht.insert(41, "block 41");
        this.ht.insert(53, "block 53");
        this.ht.insert(74, "block 74");
        this.ht.insert(88, "block 88");
        this.ht.insert(1, "block 1");
        assertEquals(25, this.ht.getCapacity()); 
        assertEquals("block 29", this.ht.removeBest(13)); 
        assertEquals(8, this.ht.getSize()); 
    } 

    /**Tests that data is moved into new buckets.*/
    @Test
    public void testRehashRemoveBest3() {
        this.ht.insert(67, "block 67");
        this.ht.insert(29, "block 29");
        this.ht.insert(88, "block 88");
        this.ht.insert(35, "block 35");
        this.ht.insert(12, "block 12");
        this.ht.insert(14, "block 14");
        this.ht.insert(18, "block 18");
        this.ht.insert(16, "block 16");
        this.ht.insert(10, "block 10");
        assertEquals(25, this.ht.getCapacity()); 
        assertEquals("block 14", this.ht.removeBest(13)); 
        this.ht.removeBest(13);
        assertEquals(7, this.ht.getSize()); 
        assertEquals("block 16", this.ht.removeBest(13)); 
    }
    
    /**Test to make sure that the hash table will rehash twice 
     * if needed.*/
    
    @Test
    public void doubleRehash() {
        this.ht.insert(67, "block 67");
        this.ht.insert(29, "block 29");
        this.ht.insert(88, "block 88");
        this.ht.insert(35, "block 35");
        this.ht.insert(12, "block 12");
        this.ht.insert(14, "block 14");
        this.ht.insert(18, "block 18");
        this.ht.insert(16, "block 16");
        this.ht.insert(57, "block 57");
        assertEquals(25, this.ht.getCapacity()); 
        this.ht.insert(59, "block 59");
        this.ht.insert(55, "block 55");
        this.ht.insert(52, "block 52");
        this.ht.insert(51, "block 51");
        this.ht.insert(53, "block 53");
        this.ht.insert(1, "block 1");
        this.ht.insert(2, "block 2");
        this.ht.insert(3, "block 3");
        this.ht.insert(4, "block 4");
        this.ht.insert(7, "block 7");
        assertEquals(53, this.ht.getCapacity()); 
        assertEquals("block 14", this.ht.removeBest(13));
        assertEquals(18, this.ht.getSize()); 
        assertEquals("block 55", this.ht.removeBest(55));
        assertEquals(17, this.ht.getSize()); 

    }


}
