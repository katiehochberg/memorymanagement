/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by everyone.

import java.util.Arrays;

/**Base Manager class. 
 * @author Kathryn
 *
 */
public class BaseManager implements MemoryManager {

    /**Counts the number of defrags.*/
    public int defrags = 0;

    /**Average time of quick sort.*/
    public long quickSortTime = 0;

    /**Average time of bucket sort.*/
    public long bucketSortTime = 0; 

    /**Keeps track of what sort time to return from average sort time. */
    public int sort = 0; 

    /**Base Manager Constructor.
     */
    public BaseManager() {

    }

    /**Method to request memory allocation.
     * @param s the size of the memory requested
     * @return true if request works, false if failed  
     */
    public int allocate(int s) {
        return 0; 
    }

    /**Method to request memory deallocation.
     * @param id the id of the allocation request
     * @return true if the request works, false if failed
     */
    public Block deallocate(int id) {
        return null; 
    }

    /**Method to partially defrag the current memory blocks. 
     * @param blocks the array of the free blocks
     * @return an array of blocks that have been partially defragmented 
     */
    public Block[] defrag(Block[] blocks) {
        Block[] quick = new Block[blocks.length];
        Block[] bucket = new Block[blocks.length];
        Block b, c; 
        for (int n = 0; n < blocks.length; n++) {
            b = new Block(blocks[n].getAddress(), blocks[n].getSize(), 
                    blocks[n].getID()); 
            quick[n] = b;
            c = new Block(blocks[n].getAddress(), blocks[n].getSize(), 
                    blocks[n].getID()); 
            bucket[n] = c;
        }
        bucket = this.bucketSort(bucket); 
        quick = this.quickSort(quick);

        int i = 0;
        int j = 1; 
        int k = 0; //will shift the blocks down as they are created 
        while (i < bucket.length) {
            while (j < bucket.length && bucket[i].getSize() 
                    + bucket[i].getAddress() == bucket[j].getAddress()) {
                bucket[i].setSize(bucket[i].getSize() + bucket[j].getSize()); 
                bucket[j] = null;
                j++; 
            }
            if (k != i) {
                bucket[k] = bucket[i];
                bucket[i] = null; 
            }
            k++;
            i = j; 
            j++; 
        }
        this.defrags++;
        return bucket; 
    }

    /**Quicksort an array. 
     * @param arrayInput the unsorted array
     * @return the sorted array
     */
    public Block[] quickSort(Block[] arrayInput) {
        long start = System.nanoTime(); 
        this.sort(arrayInput, 0, arrayInput.length - 1); 
        long end = System.nanoTime(); 
        if (arrayInput.length != 0) {
            this.quickSortTime += ((end - start) / arrayInput.length); 
        }
        return arrayInput; 
    }

    /**The sort method to be ran recursively.
     * @param array the array to be sorted (will be halved every time)
     * @param x the starting value to search the array
     * @param y the ending value to search the array
     */
    public void sort(Block[] array, int x, int y) {
        if ((y - x) < 1) {
            return;
        }
        int median = this.partitioner(array, x, y);
        this.sort(array, x, median);
        this.sort(array, median + 1, y); 
    }

    /**Function that actually performs the quicksort swaps.
     * @param b array to be sorted (will be halved every time)
     * @param begin beginning index to search the array
     * @param end ending index to search the array
     * @return the point where the front searching and back 
     * searching indexes meet 
     */
    public int partitioner(Block[] b, int begin, int end) {
        int medianPivot;
        Block temp = null;
        int i = begin;
        int j = end;
        final int k = 3;
        int[] possiblePivots = new int[k];
        possiblePivots[0] = b[begin].getAddress();
        possiblePivots[1] = b[end].getAddress();
        possiblePivots[2] = b[(end + begin) / 2].getAddress();
        Arrays.sort(possiblePivots); 
        medianPivot = possiblePivots[1]; 
        while (i < j) {
            while (b[j].getAddress() > medianPivot) {
                j--;
            }
            while (b[i].getAddress() < medianPivot) {
                i++;
            }
            temp = b[j];
            b[j] = b[i];
            b[i] = temp;
        }
        return j; //returns index value before original median 
    }

    /**Find minimum value in an array.
     * @param array the array
     * @return the minimum value
     */
    public int findMinEntry(Block[] array) {
        int min = array[0].getAddress(); 
        for (int i = 0; i < array.length; i++) {
            if (array[i].getAddress() < min) {
                min = array[i].getAddress(); 
            }
        }
        return min; 
    }

    /**Find maximum value in an array.
     * @param array the array
     * @return the minimum value
     */
    public int findMaxEntry(Block[] array) {
        int max = array[0].getAddress(); 
        for (int i = 0; i < array.length; i++) {
            if (array[i].getAddress() > max) {
                max = array[i].getAddress(); 
            }
        }
        return max; 
    }

    /**Bucketsort an array. 
     * @param array the unsorted array
     * @return the sorted array 
     */
    public Block[] bucketSort(Block[] array) {
        long start = System.nanoTime(); 
        final int numberOfBuckets = 3; 
        Block[][] buckets = new Block[numberOfBuckets][array.length];
        int index1 = 0; 
        int index2 = 0; 
        int index3 = 0; 
        int max = this.findMaxEntry(array); 
        int min = this.findMinEntry(array);
        int n = (int) Math.ceil((double) (max - min) / numberOfBuckets);    
        for (int k = 0; k < array.length; k++) {
            if ((array[k].getAddress() - min) < n) {
                buckets[0][index1] = array[k];
                index1++; 
            } else if ((array[k].getAddress() - min) >= n 
                    && (array[k].getAddress() - min) <= 2 * n) {
                buckets[1][index2] = array[k];
                index2++; 
            } else {
                buckets[2][index3] = array[k];
                index3++; 
            }
        }
        for (int j = 0; j < numberOfBuckets; j++) {
            this.selectSort(buckets[j]);
        } 
        int index = 0;
        for (int l = 0; l < numberOfBuckets; l++) {
            int m = 0; 
            while (m < array.length && buckets[l][m] != null) {
                array[index] = buckets[l][m];
                m++; 
                index++; 
            }
        }
        long end = System.nanoTime(); 
        this.bucketSortTime += ((end - start) / array.length);
        return array;
    }

    /** MODIFIED FROM SORTER.JAVA.
     * Selection sort array of strings, in descending order
      @param ra the array to sort
     */
    public void selectSort(Block[] ra) {
        Block temp;
        int minIndex;  // where largest so far is 

        for (int start = 0; start < ra.length - 1; start++) {
            if (ra[start] != null) {
                minIndex = start;
                for (int i = start + 1; i < ra.length; i++) { 
                    if (ra[i] != null) {
                        if (ra[i].getAddress() < ra[minIndex].getAddress()) {
                            minIndex = i;
                        }
                    }
                }
                // swap max with start
                temp = ra[start];
                ra[start] = ra[minIndex];
                ra[minIndex] = temp;
            }
        }
    }


    /**Method to average the size of failed allocation requests.
     * @return the number of failed allocation requests
     */
    public int avgFailSize() {
        return 0; 
    }

    /**Method to compute average time for allocation requests. 
     * @return the average time of an allocation request
     */
    public float avgTime() {
        return 0; 
    }

    /**Computes the average time for a sort. 
     * @return the average time for a sort 
     */
    public float avgSortRatio() {
        final float micro = 1000; 
        if (this.sort == 0) {
            this.sort++; 
            return ((float) this.quickSortTime / (float) this.defrags / micro);
        } else {
            this.sort--; 
            return ((float) this.bucketSortTime / (float) this.defrags / micro);
        }
    }

    /**Counts the number of failed allocation requests. 
     * @return the number of failed allocation request 
     */
    public int totalFails() {
        return 0; 
    }

    /**Counts the total number of defrags. 
     * @return the number of defrags 
     */
    public int defragCount() {
        return this.defrags; 
    }

}





