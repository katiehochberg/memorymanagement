/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Katie Hochberg. 

/**Memory Manager Interface contains memory functions such as 
 * allocate, deallocate, and defrag. 
 * @author Kathryn
 *
 */
public interface MemoryManager {

    /**Method to request memory allocation.
     * @param s the size of the memory requested
     * @return true if request works, false if failed  
     */
    int allocate(int s); 
    
    /**Method to request memory deallocation.
     * @param id the id of the allocation request
     * @return true if the request works, false if failed
     */
    Block deallocate(int id); 
    
    /**Method to partially defrag the current memory blocks. 
     * @param blocks the array of the free blocks
     * @return an array of blocks that have been partially defragmented 
     */
    Block[] defrag(Block[] blocks);

    /**Method to average the size of failed allocation requests.
     * @return the number of failed allocation requests
     */
    int avgFailSize();

    /**Method to compute average time for allocation requests. 
     * @return the average time of an allocation request
     */
    float avgTime();

    /**Computes the average time for a sort. 
     * @return the average time for a sort 
     */
    float avgSortRatio();

    /**Counts the number of failed allocation requests. 
     * @return the number of failed allocation request 
     */
    int totalFails();

    /**Counts the total number of defrags. 
     * @return the number of defrags 
     */
    int defragCount();
}
