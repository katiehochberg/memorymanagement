/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Katie Hochberg. 

/**Close fit implementation of the BaseManager class.
 * @author Kathryn
 *
 */
public class CloseFit extends BaseManager {
    
    /**The capacity of the HashTable.*/
    private int capacity; 
    
    /**The hashtable for free memory.*/
    private HashTable<Block> free; 
    
    /**An AVLTree for allocated memory.*/
    private AVLTree<Block> allocated;
    
    /**ID counter for allocations.*/
    private int id; 
    
    /**Counts the total number of fails of allocations.*/
    private int totalFails;
    
    /**Counts the fail size of failed allocations.*/
    private int failedSize;
    
    /**Keeps track of amount of time used for allocations.*/
    private long allocTime; 
    
    /**Keeps track of the memorySize.*/
    private int memorySize; 
    
    /**Constructor for CloseFit method. 
     * @param memSize the size of the memory
     */
    public CloseFit(int memSize) {
        this.capacity = memSize; 
        this.free = new HashTable<Block>(this.capacity);
        Block b = new Block(0, memSize, 0);
        this.free.insert(memSize, b);
        this.id = 0; 
        this.allocated = new AVLTree<Block>(); 
        this.memorySize = memSize; 
    }
    
    /**Method to request memory allocation.
     * @param s the size of the memory requested
     * @return true if request works, false if failed  
     */
    public int allocate(int s) {
        long start = System.nanoTime(); 
        this.id++; 
        if (s > this.memorySize) {
            this.totalFails++; 
            this.failedSize += s; 
            long end = System.nanoTime();
            this.allocTime += (end - start); 
            return -1; 
        }
        Block b = this.free.removeBest(s);
        if (b == null) {
            long beforeDefrag = System.nanoTime(); 
            this.callDefrag(); 
            long afterDefrag = System.nanoTime();
            start += (afterDefrag - beforeDefrag);
            b = this.free.removeBest(s); 
            if (b == null) {
                this.totalFails++;
                this.failedSize += s; 
                long end = System.nanoTime(); 
                this.allocTime += (end - start); 
                return -1; 
            }
        }
        if (b.getSize() > s) {
            Block c = new Block((b.getAddress() + s), (b.getSize() - s), 0);
            this.free.insert(c.getSize(), c);
            b.setSize(s); 
        }
        b.setID(this.id);
        this.allocated.insert(b, b.getID());
        long end = System.nanoTime(); 
        this.allocTime += (end - start);
        return b.getAddress(); 
    }
    
    /**Method to request memory deallocation.
     * @param iD the id of the allocation request
     * @return true if the request works, false if failed
     */
    public Block deallocate(int iD) {
        Block b = this.allocated.find(iD);
        if (b == null) {
            return null; 
        }
        this.allocated.remove(b, iD);
        b.setID(0);
        this.free.insert(b.getSize(), b);
        return b; 
    }
    
    /**Method to make an array of all the elements, call defrag, and turn back 
     * into a hashtable. 
     */
    public void callDefrag() {
        Object[] o = this.free.toArray();
        if (o.length == 0) {
            defrags++; 
            return; 
        }
        Block[] blocks = new Block[o.length]; 
        for (int i = 0; i < o.length; i++) {
            blocks[i] = (Block) o[i];
        }
        blocks = defrag(blocks);
        this.free = new HashTable<Block>(this.capacity);
        int i = 0; 
        while (i < blocks.length && blocks[i] != null) {
            this.free.insert(blocks[i].getSize(), blocks[i]);
            i++;
        }
    }
    
    /**Counts the number of failed allocation requests. 
     * @return the number of failed allocation request 
     */
    public int totalFails() {
        return this.totalFails; 
    }
    
    /**Method to average the size of failed allocation requests.
     * @return the number of failed allocation requests
     */
    public int avgFailSize() {
        if (this.totalFails == 0) {
            return 0; 
        }
        return (this.failedSize / this.totalFails);  
    }
    
    
    /**Method to compute average time for allocation requests. 
     * @return the average time of an allocation request
     */
    public float avgTime() { 
        final int toMicro = 1000;
        return ((float) this.allocTime / (float) this.id / toMicro); 
    }
    
}
