/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Allie Sanzi. 

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/*Adapted from SetTest226 from Project2.*/

/**AVLTree test.*/
public class AVLTreeTest {

    /**Set A.*/
    private static final int[] SET_A = {4,7,3,2,1,9,5,346,72,42};
    
    /**Set A with Duplicates.*/
    private static final int[] SET_A_WITH_DUPES = {4,72,346,72,72,2,1,42,5,3,7,
        9};

    /**AVL tree used for the tests.*/
    private AVLTree<Integer> t;

    /**Set up before the tests.*/
    @Before
    public void setup() {
        this.t = new AVLTree<Integer>();
    }

    /**check that a tree is empty before adding anything
     * check that a tree has size 0 before adding anything. */
    @Test
    public void testPreEmptiness() {
        assertTrue("Result", this.t.isEmpty());
        assertEquals("Result", 0, this.t.getSize());
    }

    /**Check that after inserting something, the tree isn't empty anymore
     * check that after removing that element, the tree is empty again
     * NOTE: making a tree of integers, assigned key is the same as the 
     * integer for simplicity.*/
    @Test
    public void testNonEmptiness() {
        this.t.insert(10, 10);
        assertFalse("Is no longer empty", this.t.isEmpty());
        this.t.remove(10, 10);
        assertTrue("Is empty", this.t.isEmpty());
        this.t.insert(15, 15);
        this.t.remove(15, 15);
        assertTrue("Is still empty", this.t.isEmpty());
    }

    /** size()
    add 12 elements with multiple duplicates, make sure size is 12.
    */
    @Test
    public void testAddDuplicates() {
        for (int i : SET_A_WITH_DUPES) {
            this.t.insert(i, i);
        }
        assertEquals("Size", 12, this.t.getSize());
    }        

    /** size()
    add 10 elements, make sure size is 10. 
    */
    @Test
    public void testAddTenElements() {
        for (int i : SET_A) {
            this.t.insert(i, i);
        }
        assertEquals("Result", 10, this.t.getSize());
    }

    /** remove()
    test to make sure that removing an element puts it back to 0.
    */
    @Test
    public void testRemove() {
        assertEquals(0, this.t.getSize());
        this.t.insert(25, 25);
        assertEquals(1, this.t.getSize());
        this.t.remove(25, 25);
        assertEquals(0, this.t.getSize());
        assertEquals("Not found", null, this.t.find(25));
        assertEquals(0, this.t.getSize());

    }

    /**inserts should form an unbalanced tree
     * testing right left and toString.
     */
    @Test
    public void testRotateRight() {
        this.t.insert(7, 7);
        this.t.insert(9, 9);
        this.t.insert(4, 4);
        this.t.insert(6, 6);
        this.t.insert(3, 3);
        this.t.insert(2, 2);
        System.out.println(this.t.printTree());
        assertEquals("2 3 4 6 7 9 ", this.t.printTree());

    }

    /**inserts should form an unbalanced tree
     * testing rotate left and toString.
     */
    @Test
    public void testRotateLeft() {
        this.t.insert(7, 7);
        this.t.insert(2, 2);
        this.t.insert(9, 9);
        this.t.insert(8, 8);
        this.t.insert(11, 11);
        this.t.insert(13, 13);
        assertEquals("2 7 8 9 11 13 ", this.t.printTree());
    }

    /**inserts should form an unbalanced tree
     * testing double rotate right and toString.
     */
    @Test 
    public void testDoubleRotateRight() {
        this.t.insert(10, 10);
        this.t.insert(15, 15);
        this.t.insert(5, 5);
        this.t.insert(4, 4);
        this.t.insert(7, 7);
        this.t.insert(9, 9);
        assertEquals("4 5 7 9 10 15 ", this.t.printTree());

    }

    /**inserts should form an unbalanced tree
     * testing double rotate left and toString.
     */
    @Test
    public void testDoubleRotateLeft() {
        this.t.insert(10, 10);
        this.t.insert(15, 15);
        this.t.insert(5, 5);
        this.t.insert(13, 13);
        this.t.insert(20, 20);
        this.t.insert(12, 12);
        assertEquals("5 10 12 13 15 20 ", this.t.printTree());
    }

    /**testing find best min
     * creating a tree, look for a size that isn't there, find the next biggest
     * look for a size that is there, choose it.
     */
    @Test
    public void testFindMin() {
        this.t.insert(10, 10);
        this.t.insert(15, 15);
        this.t.insert(5, 5);
        this.t.insert(13, 13);
        this.t.insert(20, 20);
        this.t.insert(12, 12);
        assertEquals("10", this.t.findBestMin(8).toString());
        assertEquals("13", this.t.findBestMin(13).toString());

    }

    /**testing  make empty
     * creating a tree, make it empty, see if size is 0.
     */
    @Test
    public void testMakeEmpty() {
        this.t.insert(10, 10);
        this.t.insert(15, 15);
        this.t.insert(5, 5);
        this.t.insert(13, 13);
        this.t.insert(20, 20);
        this.t.insert(12, 12);
        this.t.makeEmpty();
        assertEquals(0, this.t.getSize());
    }



}