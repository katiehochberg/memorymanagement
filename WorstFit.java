/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Gillian Wolff. 

/**Implementation of the worst fit method for memory storage.
 * with a heap based priority queue 
 * @author Gillian 
 *
 */
public class WorstFit extends BaseManager {

    /**Keeps track of failed allocations.*/
    int failsCount;
    
    /**Keeps track of allocated memory ID numbers.*/
    int iDCount; 
    
    /**Keeps track of the total size of blocks that 
     * failed to be allocated.*/
    int failSize;
    
    /**Keeps track of amount of time used for allocations.*/
    long allocTime; 
    
    /**A tree that stores the allocated memory blocks.*/
    AVLTree<Block> allocatedMem = new AVLTree<Block>();
    
    /**A priority queue that stores the free memory blocks.*/
    PriorityQueue<Block> freeSpace = new PriorityQueue<Block>();  
    
    /**Keeps track of the memory size.*/
    private int memorySize; 

    /**Initialized failscount, iDcount, and failsize all to 0.
     * @param memSize the size of the memory
     */
    WorstFit(int memSize) {
        this.failsCount = 0;
        this.iDCount = 0;
        this.failSize = 0;
        Block b = new Block(0, memSize, 0); 
        this.freeSpace.insert(b);
        this.memorySize = memSize; 
    }

    /**Method to request memory allocation.
     * @param s the size of the memory requested
     * @return true if request works, false if failed  
     */
    public int allocate(int s) {
        long start = System.nanoTime(); 
        this.iDCount++; 
        Block temp = this.freeSpace.returnWorst();
        if (s > this.memorySize) { 
            this.failsCount++; 
            this.failSize += s; 
            long end = System.nanoTime(); 
            this.allocTime += (end - start);
            return -1; 
        } 
        if (temp == null || temp.getSize() < s) {
            long beforeDefrag = System.nanoTime(); 
            this.callDefrag(); 
            long afterDefrag = System.nanoTime(); 
            start += (afterDefrag - beforeDefrag);
            temp = this.freeSpace.returnWorst();
            if (temp == null || temp.getSize() < s) {
                this.failsCount++; 
                this.failSize += s; 
                long end = System.nanoTime(); 
                this.allocTime += (end - start);
                return -1;
            }
        }
        //re insert the decreased size block into free memory
        this.freeSpace.remove(); 
        if (s < temp.getSize()) {
            Block c = new Block((temp.getAddress() + s),
                    (temp.getSize() - s), 0); 
            this.freeSpace.insert(c);
            temp.setSize(s);
        }
        //insert the allocated block into memory 
        temp.setID(this.iDCount);
        this.allocatedMem.insert(temp, temp.getID()); 
        long end = System.nanoTime(); 
        this.allocTime += (end - start);
        return temp.getAddress();     
    }

    /**Method to request memory deallocation.
     * @param id the id of the allocation request
     * @return true if the request works, false if failed
     */
    public Block deallocate(int id) {
        Block b = this.allocatedMem.find(id);
        if (b == null) {
            return null;
        }
        this.allocatedMem.remove(b, id);
        this.freeSpace.insert(b);
        return b; 
    }

    /**Method to partially defrag the current memory blocks. 
     */
    public void callDefrag() {
        Comparable[] t = this.freeSpace.getArray(); 
        Block[] b = new Block[t.length]; 
        for (int m = 0; m < t.length; m++) {
            b[m] = (Block) t[m];
        }
        int count = 0; 
        for (int i = 0; i < b.length; i++) {
            if (b[i] != null) {
                count++; 
            }
        }
        if (count == 0) {
            defrags++; 
            return; 
        }

        Block[] c = new Block[count]; 
        for (int n = 1; n <= count; n++) {
            c[n - 1] = b[n];
        }
        c = defrag(c);
        this.freeSpace = new PriorityQueue<Block>();
        int k = 0; 
        while (k < c.length && c[k] != null) {
            this.freeSpace.insert(c[k]); 
            k++;
        } 
    }
    
    /**Returns total fails.
     * @return total fails 
     */
    public int totalFails() {
        return this.failsCount; 
    }

    /**Returns total fail size.
     * @return total fail size 
     */
    public int avgFailSize() {
        if (this.failsCount == 0) {
            return 0; 
        }
        return (this.failSize / this.failsCount);  
    }
    
    /**Method to compute average time for allocation requests. 
     * @return the average time of an allocation request
     */
    public float avgTime() {
        final int toMicro = 1000;
        return (float) this.allocTime / (float) (this.iDCount) / toMicro; 
    }

}
