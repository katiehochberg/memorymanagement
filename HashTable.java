/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Katie Hochberg. 

/**The definition of a node that is inserted into the 
 * hash table. Each node will have a key, data of type 
 * T and a next node. This will allow for chaining within 
 * the hashtable. 
 * @author Kathryn
 *
 * @param <T>
 */
class HashNode<T> {

    /**The key of the node.*/
    private int key; 
    /**The data of the node.*/
    private T data; 
    /**The next node,.*/
    private HashNode<T> next; 

    /**Constructor of a HashNode. 
     * @param k the integer key of the node 
     * @param d the data of the node
     */
    public HashNode(int k, T d) {
        this.key = k; 
        this.data = d; 
        this.next = null; 
    }

    /**Gets the key of the node. 
     * @return the key of the node
     */
    public int getKey() {
        return this.key; 
    }

    /**Gets the data of the node. 
     * @return the data of the node 
     */
    public T getData() {
        return this.data;
    }

    /**Sets the next node. 
     * @param n the next node 
     */
    public void setNext(HashNode<T> n) {
        this.next = n; 
    }

    /**Gets the next node.
     * @return the next node
     */
    public HashNode<T> getNext() {
        return this.next; 
    }
}

/**Generic Hashtable class. 
 * @param <T>
 * @author Kathryn
 *
 */
public class HashTable<T> {

    /**The array that contains the data of the hashtable.*/
    private Object[] table; 
    /**The number of buckets in the array.*/
    private int capacity; 
    /**The number of elements in the hashtable.*/
    private int size; 
    /**The size of the buckets.*/
    private int bucketSize; 

    /**Constructor for a hashtable. 
     * @param c the capacity of the hashtable
     */
    public HashTable(int c) {
        final int k = 10; 
        this.capacity = (c / k) + 1;
        this.bucketSize = k; 
        this.table = new Object[this.capacity];
    }

    /**Insert a new node with key and data. 
     * @param key the key of the new node
     * @param data the data of the new node
     */
    public void insert(int key, T data) {
        int hash = this.hashCode(key);
        HashNode<T> n = new HashNode<T>(key, data); 
        n.setNext((HashNode<T>) this.table[hash]);
        this.table[hash] = n; 
        this.size++; 
        //Rehash the table if the load factor is > .75
        final double lf = .75; 
        if ((this.loadFactor()) >= lf) {
            this.rehash();
        }
    }

    /**Removes the easiest node to get to with a key 
     * that is >= to the given key. 
     * @param key the key to be removed
     * @return the associated data
     */
    public T removeBest(int key) {
        int hash = this.hashCode(key);
        hash = this.nextFullBucket(hash);
        if (hash >= this.capacity) {
            return null; 
        }
        HashNode<T> n = (HashNode<T>) this.table[hash];
        HashNode<T> prev = null; 
        while (n != null && n.getKey() < key) {
            prev = n; 
            n = n.getNext(); 
        } 
        if (n != null && n.getKey() >= key) {
            if (prev == null) {
                this.table[hash] = n.getNext();
            } else {
                prev.setNext(n.getNext());
            }
            this.size--; 
            return n.getData(); 
        }
        hash++;
        hash = this.nextFullBucket(hash);
        if (hash >= this.capacity) {
            return null; 
        }
        n = (HashNode<T>) this.table[hash];
        this.table[hash] = n.getNext(); 
        this.size--; 
        return n.getData(); 
    }

    /**Moves the hash to the next bucket with something in it.
     * @param hash the hash code
     * @return the index of the next bucket with something in it
     */
    public int nextFullBucket(int hash) {
        while (hash < this.capacity && this.table[hash] == null) {
            hash++;
        }
        return hash; 
    }

    /**Generates the hashCode for a given key by dividing the key by 10
     * which will put all keys in ranges of 10 in the same buckets. 
     * @param key the key 
     * @return the hashCode for the given key 
     */
    public int hashCode(int key) {
        return (key / this.bucketSize); 
    }

    /**Checks to see if the hashtable is empty. 
     * @return true if empty, else false
     */
    public boolean isEmpty() {
        for (int i = 0; i < this.capacity; i++) {
            if (this.table[i] != null) {
                return false; 
            }
        }
        return true; 
    }

    /**Computes the load factor of the hashtable. 
     * @return the load factor
     */
    public double loadFactor() {
        return ((double) this.size / this.capacity); 
    }

    /**Rehash function. 
     */
    public void rehash() {
        final int capacityMultiplier = 10; 
        HashTable<T> temp = new HashTable<T>((this.capacity + 1) * 2 
                * capacityMultiplier); 
        double t = ((double) this.bucketSize) / 2; 
        t = Math.ceil(t);
        temp.bucketSize = (int) t; 
        for (int i = 0; i < this.capacity; i++) {
            if (this.table[i] != null) {
                HashNode<T> n = (HashNode<T>) this.table[i]; 
                while (n != null) {
                    temp.insert(n.getKey(), n.getData());
                    n = n.getNext(); 
                }
            }
        } 
        this.table = temp.table;
        this.bucketSize = temp.bucketSize; 
        this.capacity = temp.capacity; 
    }

    /**Gets the number of elements in the hashtable. 
     * @return the number of elements
     */
    public int getSize() {
        return this.size; 
    }

    /**Puts all the data from the hashtable into an array.
     * @return the array version of the hashtable
     */
    public Object[] toArray() {
        Object[] t = new Object[this.size];
        int j = 0; 
        for (int i = 0; i < this.capacity; i++) {
            if (this.table[i] != null) {
                HashNode<T> n = (HashNode<T>) this.table[i];
                while (n != null) {
                    t[j] = n.getData(); 
                    j++; 
                    n = n.getNext(); 
                }
            }
        } 
        return t; 
    }

    /**Returns capacity of hash table.
     * @return capacity
     */
    public int getCapacity() {
        return this.capacity; 
    }
}


