/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Allie Sanzi. 

/** BestFit class.
 * @author alliesanzi
 */
public class BestFit extends BaseManager {


    /**The id of the block. */
    private int id = 0;

    /**New tree of free blocks indexed by size. */
    private AVLTree<Block> freeBlocks = new AVLTree<Block>();

    /**New tree of allocated blocks indexed by id. */
    private AVLTree<Block> allocatedBlocks = new AVLTree<Block>();

    /**Counts the total number of fails of allocations.*/
    private int totalFails;

    /**Counts the fail size of failed allocations.*/
    private int failedSize; 

    /**Keeps track of amount of time used for allocations.*/
    private long allocTime; 

    /**Keeps track of the memorySize of the free block.*/
    private int memorySize;

    /**Constructor for BestFit.
     * @param memSize the size of the memory
     */
    public BestFit(int memSize) {
        Block b = new Block(0, memSize, 0);
        this.freeBlocks.insert(b, memSize);
        this.memorySize = memSize; 
    }

    /**Methods that allocates a block of an input size.
     * @param size the size of the block to be allocated
     * @return true if allocation was successful, false otherwise
     */
    public int allocate(int size) {
        this.id++;
        long start = System.nanoTime(); 
        if (size > this.memorySize) {
            this.totalFails++; 
            this.failedSize += size; 
            long end = System.nanoTime(); 
            this.allocTime += (end - start);
            return -1; 
        }
        Block b = this.freeBlocks.findBestMin(size);
        if (b == null) {
            this.freeBlocks.printTree();
            long beforeDefrag = System.nanoTime(); 
            this.callDefrag();
            long afterDefrag = System.nanoTime(); 
            start += (afterDefrag - beforeDefrag);
            b = this.freeBlocks.findBestMin(size);
            if (b == null) {
                this.totalFails++;
                this.failedSize += size; 
                long end = System.nanoTime(); 
                this.allocTime += (end - start);
                return -1;
            }
        }
        this.freeBlocks.remove(b, b.getSize());
        if (b.getSize() > size) {
            Block c = new Block((b.getAddress() + size), 
                    (b.getSize() - size), 0);
            this.freeBlocks.insert(c, c.getSize());
            b.setSize(size); 
        }
        b.setID(this.id);
        this.allocatedBlocks.insert(b, b.getID());
        long end = System.nanoTime(); 
        this.allocTime += (end - start); 
        return b.getAddress(); 
    }

    /**Deallocates a block given an id number.
     * @param idNum the id
     * @return boolean if deallocation was successful, otherwise false
     */
    public Block deallocate(int idNum) {
        Block b = this.allocatedBlocks.find(idNum);
        if (b == null) {
            return null; 
        }
        this.allocatedBlocks.remove(b, idNum);
        this.freeBlocks.insert(b, b.getSize());
        return b; 
    }

    /**Calls the defrag method.
     */
    public void callDefrag() {
        Object[] o = this.freeBlocks.toArray();
        if (o == null) {
            defrags++;
            return; 
        }
        Block[] blocks = new Block[o.length];
        for (int i = 0; i < o.length; i++) {
            blocks[i] = (Block) o[i];
        }
        blocks = defrag(blocks);
        this.freeBlocks.makeEmpty(); 
        int i = 0;
        while (i < blocks.length && blocks[i] != null) {
            this.freeBlocks.insert(blocks[i], blocks[i].getSize());
            i++;
        }
    }

    /**Counts the number of failed allocation requests. 
     * @return the number of failed allocation request 
     */
    public int totalFails() {
        return this.totalFails; 
    }

    /**Method to average the size of failed allocation requests.
     * @return the number of failed allocation requests
     */
    public int avgFailSize() {
        if (this.totalFails == 0) {
            return 0; 
        }
        return (this.failedSize / this.totalFails);  
    }

    /**Method to compute average time for allocation requests. 
     * @return the average time of an allocation request
     */
    public float avgTime() {
        final float micro = 1000; 
        return ((float) this.allocTime / (float) this.id / micro); 
    }

}
