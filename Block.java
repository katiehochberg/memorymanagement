/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Allie Sanzi. 

import java.util.Comparator;

/**Memory block class.
 * Includes basic getters and setters for starting address, 
 * size, and id number
 * @author alliesanzi
 *
 */

public class Block implements Comparable<Block>, Comparator<Block> {
    
    /** the starting address of the block. */
    private int address;
    
    /** the size of the block. */
    private int size;
    
    /** the assigned ID of the block. */
    private int id;

    /**Block constructor.
     * @param a the starting address
     * @param s the size
     * @param iD the identification number
     */
    public Block(int a, int s, int iD) {
        this.address = a;
        this.size = s;
        this.id = iD;
    }
    
    /** gets the starting address of the block.
     * @return address the address
     */
    public int getAddress() {
        return this.address;
    }

    /** sets the starting address of the block. 
     * @param a the address being assigned
     */
    public void setAddress(int a) {
        this.address = a;
    }

    /** gets the size of the block.
     * @return size the size
     */
    public int getSize() {
        return this.size;
    }

    /** sets the size of the block. 
     * @param s the size to be set
     */
    public void setSize(int s) {
        this.size = s;
    }

    /** gets the ID of the block.
     * @return the ID of the block */
    public int getID() {
        return this.id;
    }

    /** sets the ID of the block. 
     * @param i the ID of the block
     */
    public void setID(int i) {
        this.id = i;
    }

    @Override
    public int compareTo(Block o) {
        return (this.size - o.getSize());
    }

    @Override
    public int compare(Block o1, Block o2) {
        return (o1.getSize() - o2.getSize());
    }
    
    /**to string method.
     * @return the string
     */
    public String toString() {
        return (this.size + " " + this.address);
    }
    
    /**Overrides equals method.
     * @param o the object to be compared
     * @return true if equals else false
     */
    public boolean equals(Object o) {
        if (!(o instanceof Block)) {
            return false;
        }
        Block b = (Block) o;
        return (this.getAddress() == b.getAddress());
    }
    
    /**Overrides the hashcode method.
     * @return the hashcode
     */
    public int hashCode() {
        return this.toString().hashCode();
    }
}
