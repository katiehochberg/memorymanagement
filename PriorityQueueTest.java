/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Katie Hochberg. 

import static org.junit.Assert.*;
import org.junit.Test; 
import org.junit.Before; 

/**Priority Queue Test class.*/
public class PriorityQueueTest {

    /**Priority queue to be used throughout test program.*/
    private PriorityQueue<Integer> pq; 
    
    /**Set up to make priority queue before the tests.*/
    @Before
    public void setup() {
        this.pq = new PriorityQueue<Integer>(); 
    }
    
    /**Test to make sure the priority queue is empty before
     * anything is added. 
     */
    @Test 
    public void preEmptiness() {
        assertTrue("Result", this.pq.isEmpty()); 
        this.pq.insert(50); 
        assertFalse("Result", this.pq.isEmpty());
    }
    
    /**Test to make sure that the elements are inserted into 
     * the priority queue correctly, and that the size is also 
     * incremented correctly. 
     */
    @Test 
    public void testInsert() {
        for (int i = 1; i <= 10; i++) {
            this.pq.insert(i);
        }
        assertFalse("Result", this.pq.isEmpty());
        assertEquals(11, this.pq.size());
    }
    
    /**Test to make sure that the remove function removes
     * the highest priority in the queue. 
     */
    @Test 
    public void testRemove() {
        for (int i = 5; i <= 50; i += 5) {
            this.pq.insert(i);
        }
        assertEquals((Integer) 50, this.pq.remove());
        assertEquals((Integer) 45, this.pq.remove());
        assertEquals((Integer) 40, this.pq.remove());
        assertEquals((Integer) 35, this.pq.remove());
        assertEquals((Integer) 30, this.pq.remove()); 
        assertEquals(6, this.pq.size());
        assertEquals((Integer) 25, this.pq.remove());
        assertEquals((Integer) 20, this.pq.remove()); 
        assertEquals((Integer) 15, this.pq.remove()); 
        assertEquals((Integer) 10, this.pq.remove()); 
        assertEquals((Integer) 5, this.pq.remove()); 
        assertTrue("Result", this.pq.isEmpty());
    }
    
    /**Test to make sure that the return worst function 
     * returns the data with the highest priority. 
     */
    @Test 
    public void testReturnWorst() {
        this.pq.insert(57);
        this.pq.insert(12);
        this.pq.insert(84);
        this.pq.insert(75);
        this.pq.insert(1);
        assertEquals((Integer) 84, this.pq.returnWorst()); 
        this.pq.remove(); 
        assertEquals((Integer) 75, this.pq.returnWorst()); 
        this.pq.remove(); 
        assertEquals((Integer) 57, this.pq.returnWorst()); 
        this.pq.remove(); 
        assertEquals((Integer) 12, this.pq.returnWorst()); 
        this.pq.remove(); 
        assertEquals((Integer) 1, this.pq.returnWorst()); 
        this.pq.remove(); 
        assertTrue("Result", this.pq.isEmpty());
    }
 
}
