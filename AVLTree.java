/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Allie Sanzi, error handling in AVL tree 
//was worked on by the rest of the group as well. 

/**Implements an AVL tree.
 * Note that all "matching" is based on the compareTo method.
 * @author Mark Allen Weiss
 * @param <T> 
 */ 
//Adapted code from Weiss textbook. 
public class AVLTree<T extends Comparable<? super T>> { 

    /** The tree root. */
    private AVLNode<T> root;

    /** the size of the tree. */
    private int size;

    /**
     * Construct the tree.
     */
    public AVLTree() {
        this.root = null;
        this.size = 0; 
    }

    /** AVLNode class.
     */
    private static class AVLNode<T> {

        /** the data in the node. */
        T data;

        /** the key (size or id) for the data (block). */
        int key;

        /** the left tree. */
        AVLNode<T> left;

        /** the right tree. */
        AVLNode<T> right;

        /** the height of the tree. */
        int height;

        /** AVLNode Constructor. 
         * @param e the data
         * @param k the key
         * @param rt the right tree
         * @param lt the left tree*/
        AVLNode(T e, int k, AVLNode<T> lt, AVLNode<T> rt) {
            this.data = e;
            this.key = k;
            this.left = lt;
            this.right = rt;
            this.height = 0;
        }
    }

    /** gets the size of the tree.
     * 
     * @return the size of the tree
     */
    public int getSize() {
        return this.size;
    }

    /**
     * Insert into the tree; duplicates are ignored.
     * @param x the item to insert.
     * @param k the key
     */
    public void insert(T x, int k) { 
        this.root = this.insert(x, k, this.root);
        this.size++;
    }

    /**
     * Remove from the tree. Nothing is done if x is not found.
     * @param x the item to remove.
     * @param k the key 
     */
    public void remove(T x, int k) {
        this.root = this.remove(x, k, this.root);
        this.size--; 
    }

    /**
     * Find the smallest item in the tree.
     * @return smallest item or null if empty.
     */
    public T findMin() {
        return this.findMin(this.root).data;
    }

  /**
     * Find an item in the tree.
     * @param k the value to search for.
     * @return the data associated with the key
     */
    public T find(int k) {
        AVLNode<T> t = this.find(k, this.root);
        if (t == null) {
            return null; 
        }
        return t.data;
    }

    /**Finds the best minimum value given a k.
     * @param k the key
     * @return the best block
     */
    public T findBestMin(int k) {
        if (this.findBestMin(k, this.root) == null) {
            return null; 
        }
        return ((this.findBestMin(k, this.root)).data);
    }

    /**
     * Make the tree logically empty.
     */
    public void makeEmpty() {
        this.root = null;
        this.size = 0;
    }

    /**
     * Test if the tree is logically empty.
     * @return true if empty, false otherwise.
     */
    public boolean isEmpty() {
        return this.root == null;
    }


    /** puts the tree into an array.
     * 
     * @return the array
     */
    public T[] toArray() {
        if (this.isEmpty()) {
            return null;
        }
        T[] array = (T[]) new Comparable[this.size];
        this.fillArray(this.root, array, 0);
        int count = 0; 
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                count++;
            }
        }
        T[] t = (T[]) new Comparable[count];
        int k = 0; 
        for (int j = 0; j < array.length; j++) {
            if (array[j] != null) {
                t[k] = array[j]; 
                k++; 
            }
        }
        return t; 
    }

    /**
     * Print the tree contents in sorted order.
     * @return the partial string 
     */
    public String printTree() {
        String s = "";
        if (this.isEmpty()) {
            s = "Empty Tree";
        } else {
            s = this.printTree(this.root);
        }
        return s;
    }

    /**Fills the array recursively.
     * @param r the root of the tree 
     * @param array the array to be filled
     * @param pos the position
     * @return the pos
     */
    private int fillArray(AVLNode<T> r, T[] array, int pos) {
        if (r.left != null) {
            pos = this.fillArray(r.left, array, pos);
        }
        array[pos] = r.data;
        pos++;
        if (r.right != null) {
            pos = this.fillArray(r.right, array, pos);
        }
        return pos;
    }

    /**
     * Internal method to insert into a subtree.
     * @param x the item to insert.
     * @param t the node that roots the tree.
     * @param k the key 
     * @return the new root.
     */
    private AVLNode<T> insert(T x, int k, AVLNode<T> t) {
        if (t == null) {
            t = new AVLNode<T>(x, k, null, null);
        } else if (k <= t.key) {
            t.left = this.insert(x, k, t.left);
            if (this.height(t.left) - this.height(t.right) == 2) {

                if (k < t.left.key) {
                    t = this.rotateWithLeftChild(t);
                } else {
                    t = this.doubleWithLeftChild(t);
                }
            }
        } else if (k > t.key) {
            t.right = this.insert(x, k, t.right);
            if (this.height(t.right) - this.height(t.left) == 2) {
                if (k > t.right.key) {
                    t = this.rotateWithRightChild(t);
                } else {
                    t = this.doubleWithRightChild(t);
                }
            }
        } 
        t.height = max(this.height(t.left), this.height(t.right)) + 1;
        return t;
    }

    /** removes from tree.
     * 
     * @param x to be removed
     * @param t the tree
     * @param k the key
     * @return the new tree
     */
    private AVLNode<T> remove(T x, int k, AVLNode<T> t) {
        if (t == null) {
            return t;   //item not found, don't do anything
        }
        if (k < t.key) {
            t.left = this.remove(x, k, t.left);
        } else if (k > t.key) {
            t.right = this.remove(x, k, t.right);
        } 
        /*if (!t.data.equals(x)) {
          t = this.findRight(t, k, x);
      }*/
        while (!(t.data.equals(x))) {
            if (t.left != null && t.left.key == k) {
                t = t.left;
            } else if (t.right != null && t.right.key == k) {
                t = t.right; 
            } else {
                return t; 
            }
        }
        if (t.left != null && t.right != null) {
            AVLNode<T> n = this.findMin(t.right);
            t.data = n.data;
            t.key = n.key; 
            t.right = this.remove(t.data, t.key, t.right);
        } else {
            if (t.left != null) {
                t = t.left;
            } else {
                t = t.right;
            }
        }
        return this.balance(t);
    }

    /**Makes sure to remove the right node, if there are duplicate 
     * keys, the node that is removed also needs to have the correct data. 
     * @param t the node that has the right key
     * @param k the key 
     * @param x the data 
     * @return the node that has the right associated data
     */
    private AVLNode<T> findRight(AVLNode<T> t, int k, T x) {
        while (!(t.data.equals(x))) {
            if (t.left != null && t.left.key == k) {
                t = t.left;
            } else if (t.right != null && t.right.key == k) {
                t = t.right; 
            } else {
                return t; 
            }
        }
        return t;
    }

    /**Balances the tree.
     * @param t the tree
     * @return the balanced tree
     */
    private AVLNode<T> balance(AVLNode<T> t) {
        if (t == null) {
            return t; 
        }
        if (this.height(t.left) - this.height(t.right) >= 1) {
            //If left or right is null its throwing a null pointer exception
            int hll, hlr; 
            if (t.left == null) {
                hll = -1; 
            } else {
                hll = this.height(t.left.left);
            }
            if (t.right == null) {
                hlr = -1; 
            } else {
                hlr = this.height(t.right.left);
            }
            if (hll >= hlr) {
                t = this.rotateWithLeftChild(t);
            } else {
                t = this.doubleWithLeftChild(t);
            }
        } 
        if (this.height(t.right) - this.height(t.left) >= 1) {
            if (this.height(t.right.right) >= this.height(t.right.left)) {
                t = this.rotateWithRightChild(t);
            } else {
                t = this.doubleWithRightChild(t);
            }
        }

        t.height = Math.max(this.height(t.left) + this.height(t.right), 0) + 1;
        return t;
    }

    /**
     * Return the height of node t, or -1, if null.
     * @param t the node
     * @return the height
     */
    private int height(AVLNode<T> t) {
        if (t == null) {
            return -1; 
        }
        return t.height;
    }

    /**
     * Internal method to find the smallest item in a subtree.
     * @param t the node that roots the tree.
     * @return node containing the smallest item.
     */
    private AVLNode<T> findMin(AVLNode<T> t) {
        if (t == null) {
            return t;
        }
        while (t.left != null) {
            t = t.left;
        }
        return t;
    }
    
   /**Internal method to find an item in a subtree.
     * @param k the key
     * @param t the node
     * @return the node if found, else null 
     */
    private AVLNode<T> find(int k, AVLNode<T> t) {
        while (t != null) {
            if (k < t.key) {
                t = t.left;
            } else if (k > t.key) {
                t = t.right;
            } else {
                return t;    // Match
            }
        }
        return null;   // No match
    }

    /** finds the best minimum value given a min size. 
     * @param k the key
     * @param t the block
     * @return the best block
     */
    private AVLNode<T> findBestMin(int k, AVLNode<T> t) {
        AVLNode<T> p = null; 
        while (t != null) {
            if (k > t.key) {
                t = t.right; 
            } else if (k == t.key) {
                return t; 
            } else {
                while (t.left != null && t.left.key >= k) {
                    t = t.left; 
                }
                p = t; 
                t = t.left; 
                if (t == null || t.right == null) {
                    return p; 
                }
            }
        }
        return p; 
    }

    /**
     * Internal method to print a subtree in sorted order.
     * @param t the node that roots the tree.
     * @return the string of the tree
     */
    private String printTree(AVLNode<T> t) {
        String s = "";
        if (t != null) {
            s += this.printTree(t.left);
            s += t.data + " ";
            s += this.printTree(t.right);
        }
        return s;
    }

    /**
     * Return maximum of lhs and rhs.
     * @param lhs the left
     * @param rhs the right
     * @return the maximum 
     */
    private static int max(int lhs, int rhs) {
        if (lhs > rhs) {
            return lhs;
        } else {
            return rhs;
        }
    }

    /**
     * Rotate binary tree node with left child.
     * For AVL trees, this is a single rotation for case 1.
     * Update heights, then return new root.
     * @param k2 the tree
     * @return the new node
     */
    private AVLNode<T> rotateWithLeftChild(AVLNode<T> k2) {
        AVLNode<T> k1 = k2.left;
        k2.left = k1.right;
        k1.right = k2;
        k2.height = max(this.height(k2.left), this.height(k2.right)) + 1;
        k1.height = max(this.height(k1.left), k2.height) + 1;
        return k1;
    }

    /**
     * Rotate binary tree node with right child.
     * For AVL trees, this is a single rotation for case 4.
     * Update heights, then return new root.
     * @param k1 the tree
     * @return the tree
     */
    private AVLNode<T> rotateWithRightChild(AVLNode<T> k1) {
        if (k1.right == null) {
            return k1; 
        }
        AVLNode<T> k2 = k1.right;
        k1.right = k2.left;
        k2.left = k1;
        k1.height = max(this.height(k1.left), this.height(k1.right)) + 1;
        k2.height = max(this.height(k2.right), k1.height) + 1;
        return k2;
    }

    /**
     * Double rotate binary tree node: first left child
     * with its right child; then node k3 with new left child.
     * For AVL trees, this is a double rotation for case 2.
     * Update heights, then return new root.
     * @param k3 the tree
     * @return the tree
     */
    private AVLNode<T> doubleWithLeftChild(AVLNode<T> k3) {
        k3.left = this.rotateWithRightChild(k3.left);
        return this.rotateWithLeftChild(k3);
    }

    /**
     * Double rotate binary tree node: first right child
     * with its left child; then node k1 with new right child.
     * For AVL trees, this is a double rotation for case 3.
     * Update heights, then return new root.
     * @param k1 the tree
     * @return the tree
     */
    private AVLNode<T> doubleWithRightChild(AVLNode<T> k1) {
        k1.right = this.rotateWithLeftChild(k1.right);
        return this.rotateWithRightChild(k1);
    }
} 