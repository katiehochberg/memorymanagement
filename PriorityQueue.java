/*Kathryn Hochberg, Alianna Sanzi, Gillian Wolff 
 * khochbe1, asanzi1, gwolff1
 * 600.226 
 * Project 4
 */

//Worked on by Gillian Wolff. 

/**Maximum priority heap with array implementation. 
 * @author Gillian Wolff
 * @param <T>
 *
 */
public class PriorityQueue<T extends Comparable<T>> { 

    /**Array that stores and manages the heap.*/
    private T[] arrayHeap;  

    /**Keeps track of index and number of elements in heap.*/
    private int count = 1; 

    /**Constructor initializes the array heap.*/
    public PriorityQueue() {
        this.arrayHeap = (T[]) new Comparable[2]; 
    }

    /**Get the array of the heap.
     * @return the array
     */
    public T[] getArray() {
        return this.arrayHeap; 
    }

    /**Checks to see if heap is empty.
     * @return true is empty, false if not
     */
    public boolean isEmpty() {
        if (this.count == 1) {
            return true;
        }
        return false; 
    }

    /**Return the size of the array.
     * @return this.count
     */
    public int size() {
        return this.count; 
    }

    /**Inserts data into heap and maintains heap descending order.
     * @param d information to be added
     */
    public void insert(T d) {
        if (this.isEmpty()) {
            this.arrayHeap[this.count] = d;
            this.count++; 
            return;
        }

        int tempCount;
        T tempD = null; 

        if (this.arrayHeap.length == this.count) {
            T[] temp = (T[]) new Comparable[this.count * 2]; 

            for (int i = 0; i < this.count; i++) {
                temp[i] = this.arrayHeap[i];
            }
            this.arrayHeap = temp; 
        }

        this.arrayHeap[this.count] = d;
        tempCount = this.count;

        while (tempCount / 2 >= 1 
                && this.arrayHeap[tempCount]
                        .compareTo(this.arrayHeap[tempCount / 2]) > 0) {
            tempD = this.arrayHeap[tempCount];
            this.arrayHeap[tempCount] = this.arrayHeap[tempCount / 2];
            this.arrayHeap[tempCount / 2] = tempD;
            tempCount = tempCount / 2; 
        } 
        this.count++;
    }

    /**Removes the root (highest value) from the heap.
     * @return data that was removed
     */
    public T remove() {
        int tempCount = 1;
        T tempD; 
        T tempReturn;
        tempReturn = this.arrayHeap[tempCount];
        this.arrayHeap[tempCount] = this.arrayHeap[this.count - 1];
        this.arrayHeap[this.count - 1] = null; 
        while ((tempCount * 2 + 1) < this.count && this.arrayHeap[tempCount 
                                                                  * 2 + 1] 
                != null && (this.arrayHeap[tempCount]
                        .compareTo(this.arrayHeap[tempCount * 2]) < 0 
                        || this.arrayHeap[tempCount]
                                .compareTo(this.arrayHeap[tempCount * 2 + 1]) 
                                < 0)) {
            if (this.arrayHeap[tempCount * 2 + 1]
                    .compareTo(this.arrayHeap[tempCount * 2]) > 0) {
                tempD = this.arrayHeap[tempCount];
                this.arrayHeap[tempCount] = this.arrayHeap[tempCount * 2 + 1];
                this.arrayHeap[tempCount * 2 + 1] = tempD;
                tempCount = tempCount * 2 + 1;
            } else {
                tempD = this.arrayHeap[tempCount];
                this.arrayHeap[tempCount] = this.arrayHeap[tempCount * 2];
                this.arrayHeap[tempCount * 2] = tempD;
                tempCount = tempCount * 2; 
            }
        }
        this.count--; 
        return tempReturn;
    }

    /**Returns largest values in heap.
     * @return largest value in heap
     */
    public T returnWorst() {
        if (this.isEmpty()) {
            return null;
        }
        return this.arrayHeap[1];
    }

    /**Overrides to string function to print priority queue. 
     * @return modified string 
     */
    public String toString() {
        StringBuffer s = new StringBuffer("");
        s.append("[");
        for (int i = 1; i < this.count; i++) {
            s.append(this.arrayHeap[i] + " ");
        }
        s.append("]"); 
        return s.toString(); 
    }

}


